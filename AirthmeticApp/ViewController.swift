//
//  ViewController.swift
//  AirthmeticApp
//
//  Created by student on 2/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var ActivityTF: UITextField!
    
    @IBOutlet weak var WeightTF: UITextField!
    
    @IBOutlet weak var ExerciseTF: UITextField!
    
    @IBOutlet weak var EnergyConsumedLBL: UILabel!
    
    @IBOutlet weak var TimeLBL: UILabel!
    @IBOutlet weak var ErrorLBL: UILabel!
    @IBAction func caluculator(_ sender: Any) {
        if let act = ActivityTF.text  {
            if let a = ExerciseCoach.sports[act] {
                EnergyConsumedLBL.text = String(format:"%.1f cal", ExerciseCoach.energyConsumed(during: act, weight: Double(WeightTF.text!)!, time: Double(ExerciseTF.text!)!))
                TimeLBL.text = String(format:"%.1f  minutes", ExerciseCoach.timeToLose1Pound(during: act, weight: Double(WeightTF.text!)!))
                clearError()
                return
            }
        }
        ErrorLBL.text="Please Enter a valid Inputs"
        
        
    }
    func clearError(){
        ErrorLBL.text=""
    }
    
    @IBAction func clear(_ sender: Any) {
        ActivityTF.text! = " "
        WeightTF.text! = " "
        ExerciseTF.text! = " "
        EnergyConsumedLBL.text = "0 cal"
        TimeLBL.text = "0 minutes"
        clearError()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        clearError()
    }


}

