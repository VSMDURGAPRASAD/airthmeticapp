//
//  ExerciseCoach.swift
//  AirthmeticApp
//
//  Created by student on 2/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
struct  ExerciseCoach{
    static let sports:[String:Double]=["Bicycling":8.0,"Jumping rope":12.3,"Running - slow":9.8,"Running - fast":23.0,"Tennis":8.0,"Swimming":5.8]
    static func energyConsumed(during:String,weight:Double,time:Double) -> Double {
        let er=0.0
        if let er  = ExerciseCoach.sports[during]{
            return er * 3.5 * ((weight/2.2) / 200) * time
        }
        return er
       
        //MET x 3.5 x weight(kg) / 200 * time
        
    }
    static func timeToLose1Pound(during: String,weight:Double) -> Double {
       //  if let Met=MET x 3.5 x weight(kg)/ 200
        let MET=0.0
        if let MET=ExerciseCoach.sports[during]{
            return 3500/ExerciseCoach.energyConsumed(during: during, weight: weight, time: 1)
        }
        return MET
        
    }
    
    
    
}
